**Hungry Rain**

A tiny survival game for the YouMakeTech Raspberry Pi Pico Game Boy MicroPython DIY handheld.
Find food. Hide from the flood.

Requires st7789.py and PicoGameBoy.py from https://github.com/YouMakeTech/Pi-Pico-Game-Boy

apple.png and elementStone013.png from Kenney Game Assets 1

The graphics are supplied as an example and are not required to build code.

To convert PNG graphics to bytearrays for use with PicoGameBoy, save as RGB565-compatible 8bpc RGB PNG files then use png2fb.py from https://github.com/YouMakeTech/Pi-Pico-Game-Boy